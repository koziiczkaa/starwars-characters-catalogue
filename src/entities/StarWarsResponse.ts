export interface StarWarsResponse<T> {
	results: T;
	next: string;
}
