import React, {useEffect} from 'react';
import Axios, {AxiosResponse} from 'axios';
import {Character} from '../entities/Character';
import {SingleCharacter} from './SingleCharacter';
import {StarWarsResponse} from '../entities/StarWarsResponse';
import {LoadingBar} from './LoadingBar';
import {FilteringInput} from './FilteringInput';

export function CharactersList(): JSX.Element {
	const [characters, setCharacters] = React.useState<Character[]>([]);
	const [filteredCharacters, setFilteredCharacters] = React.useState<Character[]>([]);
	const [nextPageUrl, setNextPageUrl] = React.useState<string>('https://swapi.dev/api/people');
	const [loading, setLoading] = React.useState<boolean>(false);

	useEffect(fetchData, []);

	useEffect((): () => void => {
		function doIt(): void {
			if (loading) {
				return;
			}
			if (window.scrollY + 2000 > document.documentElement.getBoundingClientRect().height) {
				fetchData();
			}
		}

		window.addEventListener('scroll', doIt);
		return (): void => window.removeEventListener('scroll', doIt);
	}, [nextPageUrl, loading]);

	function onFiltering(value: string): void {
		console.log(characters, filteredCharacters);
		if (value === '') {
			setFilteredCharacters(characters);
			console.log('if');
		} else {
			setFilteredCharacters(characters.filter((character: Character): boolean => character.name.includes(value)));
			console.log('else');
		}
	}

	function fetchData(): void {
		setLoading(true);
		if (characters.length !== filteredCharacters.length) {
			return;
		}
		Axios.get(nextPageUrl)
			.then((response: AxiosResponse<StarWarsResponse<Character[]>>): void => {
				setCharacters([...characters, ...response.data.results]);
				setNextPageUrl(response.data.next);
				setFilteredCharacters([...characters, ...response.data.results]);
			})
			.catch((): void => console.error('Cannot fetch data!'))
			.finally((): void => setLoading(false));
	}

	return <>
		{loading && <LoadingBar/>}
		<FilteringInput onChange={onFiltering}/>
		{filteredCharacters.map((character: Character, index: number): JSX.Element =>
			<SingleCharacter character={character} key={index} id={index + 1}/>
		)}
	</>;
}
